from tkinter import *
from PIL import ImageTk, Image
from math import sin,exp
import webbrowser
class Integral():

    def __init__(self):

        self.root = Tk()
        self.loadingimage()
        self.root.title('Integral')
        self.root.geometry('800x600')
        self.root.protocol('WM_DELETE_WINDOW')  # обработчик закрытия окна
        self.root.resizable(False, False)  # размер окна нельзя изменить

        self.body = Frame(self.root,width=400, height=60,bd=5)
        self.body1 = Frame(self.root, width=800, height=540, bd=5)
        self.inpyt_frame = Frame(self.body1, width=200, height=40, bd=5)
        self.result_frame = Frame(self.body1, width=600,bg='grey', height=240, bd=5)


        self.button0 = Button(self.body,  width=380, height=60, fg='red', font='arial 14', image=self.listImage[0],state="normal",command=lambda number_function=0:self.click(number_function))
        self.button1 = Button(self.body, width=380, height=60, fg='red', font='arial 14', image=self.listImage[1],state="normal",command=lambda number_function=1:self.click(number_function))
        self.predelB = [Label(self.inpyt_frame, text='Введите верхний предел', font='arial 15',),Entry(self.inpyt_frame,width=4,state="normal")]
        self.predelA = [Label(self.inpyt_frame, text='Введите нижний предел', font='arial 15', ),Entry(self.inpyt_frame, width=4, state="normal")]
        self.error_predel = Label(self.inpyt_frame, text = "*Верхний предел\n должен быть > нижнего",state="disabled")
        self.result_button = Button(self.body1, text="посчитать", font='arial 14', state="disabled", command=lambda number_function=0:self.click__get())
        self.error_k = Label(self.inpyt_frame, text="*k не может\n быть равен 0", state="disabled")
        self.rezultat = Text(self.body1,height=1,width=25,font='Arial 25',wrap=WORD)
        self.step_scala_labol = Label(self.body1,text="выберите точность\n  (количество шагов)",)
        self.step_scala = Scale(self.body1, orient=HORIZONTAL, length=220, from_=5, to=10000, resolution=5)
        self.button_help = Button(self.body1, text="помощь",command=lambda nt=0:self.call_help())




        self.labol_list = []
        self.leater_list = ['a','n','m','k','d','g']

        for i in range(6):
            self.labol_list.append(Label(self.inpyt_frame, text='Введите '+self.leater_list[i], font='arial 15'))

        self.entry_list = []
        for i in range(6):
            self.entry_list.append(Entry(self.inpyt_frame,width=4,state="normal",bg='#FFFFFF',))

        #self.create_grafic()

        self.body.pack()
        self.button0.grid(row=1,column=0)
        self.button1.grid(row=1,column=1)

        self.body1.pack()
        self.rezultat.grid(row=1,column=1)
        self.inpyt_frame.grid(row=0, column=0)

        self.result_button.grid(row=3)
        self.button_help.grid(row=3,column=1)
        self.step_scala_labol.grid(row=1)
        self.step_scala.grid(row=2)
        """for i in range(7):
            self.labol_list[i].grid(row=i, column=0)
            self.entry_list[i].grid(row=i, column=1)"""
        self.predelA[1].grid(row=8, column=1)
        self.predelA[0].grid(row=8, column=0)
        self.predelB[1].grid(row=9, column=1)
        self.predelB[0].grid(row=9, column=0)
        self.error_predel.grid(row=10, column=0)
        self.error_predel.grid_remove()
        self.create_grafic_body()
        self.root.mainloop()
    def call_help(self):
        print("help")
        webbrowser.open('https://pp.userapi.com/c837331/v837331923/2443f/1HQrqxHX_6U.jpg')
    def f1(self,x):
        return ((sin(x*self.save_entry_dict["a"])**self.save_entry_dict["n"])*x**(self.save_entry_dict["m"]/self.save_entry_dict["k"]))

    def f2(self, x,d,g,n,m,k):
        return d*exp((-(g)*x**n))*x**(m/k)
    def click__get(self):
        self.result_canvas_grafic.delete("group1")
        self.error_predel.grid_remove()
        self.error_k.grid_remove()
        self.save_predel = self.predelA[1].get()
        self.save_predel = self.predelB[1].get()
        self.save_entry_dict = { "a" : None , "n": None, "m": None, "k": None, "d": None, "e": None, "g": None, 'predelB': None, 'predelA': None }
        self.correct_inpyt = 0
        for i in range(6):
            if self.entry_list[i].get()=='':
                self.entry_list[i].configure(bg='#FF2130')
            else:
                if self.entry_list[i].get().isdigit():
                    self.save_entry_dict[self.leater_list[i]]=float(self.entry_list[i].get())
                    self.entry_list[i].configure(bg='#FFFFFF')
                    self.correct_inpyt += 1
                else:
                    self.entry_list[i].configure(bg='#FF2130')
        try:
            if int(self.predelB[1].get()) > int(self.predelA[1].get()):
                if self.predelB[1].get().isdigit():
                    self.save_entry_dict['predelB'] = float(self.predelB[1].get())
                    self.predelB[1].configure(bg='#FFFFFF')

                else:
                    self.predelB[1].configure(bg='#FF2130')
                if self.predelA[1].get().isdigit():
                    self.save_entry_dict['predelA'] = float(self.predelA[1].get())
                    self.predelA[1].configure(bg='#FFFFFF')
                else:
                    self.predelA[1].configure(bg='#FF2130')
            else:
                self.error_predel.grid(row=11, column=0)
                self.predelA[1].configure(bg='#FF2130')
                self.predelB[1].configure(bg='#FF2130')
        except ValueError:
            self.predelB[1].configure(bg='#FF2130')
            self.predelA[1].configure(bg='#FF2130')

        if self.save_entry_dict['k']==0:
            self.error_k.grid(row=10, column=0)
            self.entry_list[3].configure(bg="red")

        print(self.save_entry_dict)

        if self.check_correct_input() == 1:
            n = self.step_scala.get()  # количество трапеуий на которые разобьеться функция
            self.calculations(n,self.save_entry_dict["predelA"],self.save_entry_dict["predelB"],self.number_function)


    def check_correct_input(self):
        if self.number_function == 0:
            if self.save_entry_dict['a'] != None and self.save_entry_dict['n'] != None and self.save_entry_dict['m'] != None and self.save_entry_dict['k'] != None and self.save_entry_dict['predelA'] != None and self.save_entry_dict['predelB'] != None:
                if self.save_entry_dict['predelB']>self.save_entry_dict['predelA']:
                    return 1
                else:
                    return 0
            else:
                return 0
        if self.number_function == 1:
            if self.save_entry_dict['n'] != None and self.save_entry_dict['m'] != None and self.save_entry_dict['k'] != None and self.save_entry_dict['d'] != None and self.save_entry_dict['g'] != None and self.save_entry_dict['predelA'] != None and self.save_entry_dict['predelB'] != None:
                if self.save_entry_dict['predelB']>self.save_entry_dict['predelA']:
                    return 1
                else:
                    return 0
            else:
                return 0


    def click(self,number_function):

        self.rezultat.delete('1.0', END)
        self.number_function = number_function
        self.result_button.configure(state="normal")
        if(number_function == 0):
            self.button0.configure(state="disabled")
            self.button1.configure(state="normal")
            for i in range(4):
                self.labol_list[i].grid(row=i, column=0)
                self.entry_list[i].grid(row=i, column=1)
            for i in range(4,6):
                self.labol_list[i].grid_remove()
                self.entry_list[i].grid_remove()
        elif(number_function == 1):
            self.button1.configure(state="disabled")
            self.button0.configure(state="normal")
            for i in range(1,6):
                self.labol_list[i].grid(row=i, column=0)
                self.entry_list[i].grid(row=i, column=1)
            self.labol_list[0].grid_remove()
            self.entry_list[0].grid_remove()


        else:
            print("error click")
            self.result_button.configure(state="disabled")


    def calculations(self,n,a,b,number_function):
        h=(b-a)/n#шаг

        if number_function == 0:
            rez = (self.f1(a)+self.f1(b))/2
            self.grafic_coordinat_list = [rez*h, a, (rez+self.f1(a+h))*h, a+h]
            print("rez",rez)
            for i in range(n-1):
                a+=h
                rez+=self.f1(a)
                #print("rez"+str(i),rez)
                x=0
                if(x%2==0):
                    self.draw_grafic(self.grafic_coordinat_list[0], self.grafic_coordinat_list[1] * h, self.grafic_coordinat_list[2], self.grafic_coordinat_list[3] * h)
                    self.grafic_coordinat_list[0]=self.grafic_coordinat_list[2]
                    self.grafic_coordinat_list[1]=self.grafic_coordinat_list[3]
                    self.grafic_coordinat_list[2] = a
                    self.grafic_coordinat_list[3] = rez
                    print(self.grafic_coordinat_list)
                    x=0
                else:
                    self.grafic_coordinat_list[0] = self.grafic_coordinat_list[2]
                    self.grafic_coordinat_list[1] = self.grafic_coordinat_list[3]
                    self.grafic_coordinat_list[2]=a
                    self.grafic_coordinat_list[3]=rez
                    x+=1


            rez*=h
            print(rez,a)
        elif number_function == 1:
            rez = (self.f2(a,self.save_entry_dict["d"],self.save_entry_dict["g"],self.save_entry_dict["n"],self.save_entry_dict["m"],self.save_entry_dict["k"]) + self.f2(b,self.save_entry_dict["d"],self.save_entry_dict["g"],self.save_entry_dict["n"],self.save_entry_dict["m"],self.save_entry_dict["k"])) / 2
            self.grafic_coordinat_list = [rez, a, rez, a]
            print("rez", rez)
            for i in range(n - 1):
                a += h
                rez += self.f2(a,self.save_entry_dict["d"],self.save_entry_dict["g"],self.save_entry_dict["n"],self.save_entry_dict["m"],self.save_entry_dict["k"])
                x = 0
                if (x % 2 == 0):
                    self.draw_grafic(self.grafic_coordinat_list[0], self.grafic_coordinat_list[1] * h,
                                     self.grafic_coordinat_list[2], self.grafic_coordinat_list[3] * h)
                    self.grafic_coordinat_list[0] = self.grafic_coordinat_list[2]
                    self.grafic_coordinat_list[1] = self.grafic_coordinat_list[3]
                    self.grafic_coordinat_list[2] = a
                    self.grafic_coordinat_list[3] = rez
                    print(self.grafic_coordinat_list)
                    x = 0
                else:
                    self.grafic_coordinat_list[0] = self.grafic_coordinat_list[2]
                    self.grafic_coordinat_list[1] = self.grafic_coordinat_list[3]
                    self.grafic_coordinat_list[2] = a
                    self.grafic_coordinat_list[3] = rez
                    x += 1
            rez *= h
            print(rez)
        self.rezultat.delete('1.0', END)  # Удалить все
        if rez < 0:
            rez=rez*-1.0
        self.rezultat.insert(1.0, rez)






    def loadingimage(self):
        self.pilImage = Image.open("a1.png")
        self.pilImage1 = Image.open("b1.png")

        self.listImage = [ImageTk.PhotoImage(self.pilImage,width=100, height=30),ImageTk.PhotoImage(self.pilImage1,width=100, height=30)]

        self.pilImage.close()
        self.pilImage1.close()
    def create_grafic_body(self):
        self.result_canvas_grafic = Canvas(self.body1, width=600,  height=390, bd=5,)
        self.result_canvas_grafic.create_line(20, 400, 20, 20, width=2, arrow=LAST)
        self.result_canvas_grafic.create_line(20, 200, 500, 200, width=2, arrow=LAST)
        self.result_canvas_grafic.grid(row=0, column=1)
    def draw_grafic(self,x,y,x1,y1):

        print("test")
        b=300+x
        a=300+y
        self.result_canvas_grafic.create_line(x+20, y+200, x1+20, y1+200, width=2,tag="group1",fill = 'red')
        #self.result_canvas_grafic.create_oval(x+20, y+200, x+23, y+203,tag="group1",  fill='red',outline="red")


Integral()
